﻿using System;
using System.Collections.Generic;

namespace RomanNumbers
{
    class Program
    {
        Dictionary<int, string> dict = new Dictionary<int, string>();

        static void Main(string[] args)
        {
            Program p = new Program();
            p.AssignDict();
            int num = p.GetInput();
            Console.WriteLine("Roman number: " + p.MakeRoman(num));
        }

        public void AssignDict()
        {
            dict.Add(1, "I");
            dict.Add(2, "II");
            dict.Add(3, "III");
            dict.Add(4, "IV");
            dict.Add(5, "V");
            dict.Add(6, "VI");
            dict.Add(7, "VII");
            dict.Add(8, "VIII");
            dict.Add(9, "XI");
            dict.Add(10, "X");
            dict.Add(20, "XX");
            dict.Add(30, "XXX");
            dict.Add(40, "XL");
            dict.Add(50, "L");
            dict.Add(60, "LX");
            dict.Add(70, "LXX");
            dict.Add(80, "LXXX");
            dict.Add(90, "XC");
            dict.Add(100, "C");
            dict.Add(200, "CC");
            dict.Add(300, "CCC");
            dict.Add(400, "CD");
            dict.Add(500, "D");
            dict.Add(600, "DC");
            dict.Add(700, "DCC");
            dict.Add(800, "DCCC");
            dict.Add(900, "CM");
            dict.Add(1000, "M");
            dict.Add(2000, "MM");
            dict.Add(3000, "MMM");
            dict.Add(4000, "MMMM");
        }

        public int GetInput()
        {
            Console.WriteLine("Please give numerical input: ");
            return Int32.Parse(Console.ReadLine());
        }

        public string MakeRoman(int num)
        {
            Stack<int> stack = new Stack<int>();
            string roman = "";
            int x = 1;
            while (num > 0)
            {
                int digit = num % 10;
                stack.Push(digit * x);
                x = x * 10;
                num = num / 10;
            }
            while (stack.Count != 0)
            {
                int digit = stack.Pop();
                if (digit != 0)
                {
                    roman += dict.GetValueOrDefault(digit);
                }
            }
            return roman;
        }
    }
}
